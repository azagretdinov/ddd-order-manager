package com.bitbucket.azagretdinov.dddordermanager;

import com.bitbucket.azagretdinov.dddordermanager.anticorruption.bank.messaging.BankBindings;
import com.bitbucket.azagretdinov.dddordermanager.anticorruption.portfoliomanagement.messaging.PortfolioManagementStream;
import com.bitbucket.azagretdinov.dddordermanager.order.messaging.OrderBindings;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.messaging.PortfolioBindings;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableBinding({
        PortfolioManagementStream.class,
        PortfolioBindings.class,
        OrderBindings.class,
        BankBindings.class
})
public class DddOrderManagerApplication {
	public static void main(String[] args) {
		SpringApplication.run(DddOrderManagerApplication.class, args);
	}

    @Bean
    public Jackson2JsonMessageConverter jackson2JsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
