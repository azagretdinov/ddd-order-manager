package com.bitbucket.azagretdinov.dddordermanager.anticorruption.portfoliomanagement.messaging;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortfolioOpened {
    private String referenceIdentifier;
    private String clientRef;
    private String type;

    private String riskRate;
    private String clientRiskRate;
    private AccountNumber cashAccountNumber;
    private String custodyAccountNumber;

    public PortfolioOpened(){

    }
}
