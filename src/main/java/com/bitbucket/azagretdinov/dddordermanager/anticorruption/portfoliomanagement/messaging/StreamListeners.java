package com.bitbucket.azagretdinov.dddordermanager.anticorruption.portfoliomanagement.messaging;

import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CashAccountDetails;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CustodyAccountDetails;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.service.PortfolioService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class StreamListeners {

    private final PortfolioService portfolioService;

    public StreamListeners(PortfolioService portfolioService) {
        this.portfolioService = portfolioService;
    }

    @StreamListener(
            target= PortfolioManagementStream.PORTFOLIO_EVENTS,
            condition = "headers['eventType']=='portfolio-opened'"
    )
    public void portfolioOpened(PortfolioOpened portfolioOpened){
        portfolioService.newPortfolio(
                portfolioOpened.getReferenceIdentifier(),
                new CashAccountDetails(
                        portfolioOpened.getCashAccountNumber().getAccountNumber(),
                        portfolioOpened.getCashAccountNumber().getSortCode()
                ),
                new CustodyAccountDetails(
                        portfolioOpened.getCustodyAccountNumber()
                ),
                portfolioOpened.getClientRiskRate(),
                portfolioOpened.getRiskRate()
        );
    }

    @StreamListener(
            target=PortfolioManagementStream.PORTFOLIO_EVENTS,
            condition = "headers['eventType']!='portfolio-opened'"
    )
    public void otherEvents(@Header("eventType") String eventType, @Payload byte[] payload){
        log.info("Received event of type {} from portfolio manager: {}", eventType, new String(payload));
    }


}
