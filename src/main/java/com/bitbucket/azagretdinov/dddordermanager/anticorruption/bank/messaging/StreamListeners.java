package com.bitbucket.azagretdinov.dddordermanager.anticorruption.bank.messaging;

import com.bitbucket.azagretdinov.dddordermanager.order.command.SecureCash;
import com.bitbucket.azagretdinov.dddordermanager.anticorruption.bank.event.CashSecured;
import com.bitbucket.azagretdinov.dddordermanager.order.messaging.OrderBindings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component("bankStreamListeners")
@Slf4j
public class StreamListeners {

    private MessageChannel bankEvents;

    public StreamListeners(BankBindings bankBindings) {
        this.bankEvents = bankBindings.bankEvents();
    }

    @StreamListener(
            target= OrderBindings.ORDER_COMMAND,
            condition = "headers['eventType']=='secure-cash'"
    )
    public void secureCash(SecureCash secureCash){
       log.info("Receive {}", secureCash);
        final Message<CashSecured> msg = MessageBuilder.withPayload(
                new CashSecured(secureCash.getOrderId(), secureCash.getCashAmount())
        )
                .setHeader("eventType", "cash-secured")
                .build();
        log.info("publish {}", msg);
        bankEvents.send(msg);
    }

}
