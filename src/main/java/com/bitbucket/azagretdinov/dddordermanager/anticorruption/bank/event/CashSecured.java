package com.bitbucket.azagretdinov.dddordermanager.anticorruption.bank.event;

import lombok.ToString;

import java.math.BigDecimal;

@ToString
public class CashSecured {
    private String orderId;
    private BigDecimal cashAmount;

    public CashSecured(String orderId, BigDecimal cashAmount) {
        this.orderId = orderId;
        this.cashAmount = cashAmount;
    }

    public BigDecimal getCashAmount() {
        return cashAmount;
    }


    public String getOrderId() {
        return orderId;
    }


    public String getType() {
        return "cash-secured";
    }
}
