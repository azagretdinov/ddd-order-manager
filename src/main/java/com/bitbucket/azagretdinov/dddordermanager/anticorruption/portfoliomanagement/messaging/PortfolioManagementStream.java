package com.bitbucket.azagretdinov.dddordermanager.anticorruption.portfoliomanagement.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface PortfolioManagementStream {
    String PORTFOLIO_EVENTS = "portfolio-events";

    @Input(PORTFOLIO_EVENTS)
    SubscribableChannel portfolioEvents();
}
