package com.bitbucket.azagretdinov.dddordermanager.anticorruption.portfoliomanagement.messaging;

import lombok.Builder;
import lombok.Data;


@Data
public class AccountNumber {
    private String sortCode;
    private String accountNumber;
    private String swiftCode;

    public AccountNumber(){

    }

    @Builder
    public AccountNumber(String sortCode, String accountNumber, String swiftCode) {
        this.sortCode = sortCode;
        this.accountNumber = accountNumber;
        this.swiftCode = swiftCode;
    }
}
