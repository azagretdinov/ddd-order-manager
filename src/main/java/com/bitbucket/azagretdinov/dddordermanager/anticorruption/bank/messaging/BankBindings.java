package com.bitbucket.azagretdinov.dddordermanager.anticorruption.bank.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface BankBindings {
    String BANK_EVENTS = "bank-events";

    @Input(BANK_EVENTS)
    SubscribableChannel bankEvents();
}
