package com.bitbucket.azagretdinov.dddordermanager.portfolio.model;

import lombok.Getter;

@Getter
public class CustodyAccountDetails {
    private String custodyAccountNumber;

    public CustodyAccountDetails(String custodyAccountNumber) {
        this.custodyAccountNumber = custodyAccountNumber;
    }
}
