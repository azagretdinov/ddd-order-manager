package com.bitbucket.azagretdinov.dddordermanager.portfolio.model;

public enum PortfolioRiskRate {
    Low,
    High,
    Critical
}
