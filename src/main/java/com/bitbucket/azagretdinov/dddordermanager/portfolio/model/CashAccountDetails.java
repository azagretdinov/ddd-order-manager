package com.bitbucket.azagretdinov.dddordermanager.portfolio.model;

import lombok.Data;

@Data
public class CashAccountDetails {
    private final String accountNumber;
    private final String sortCode;

    public CashAccountDetails(String accountNumber, String sortCode) {
        this.accountNumber = accountNumber;
        this.sortCode = sortCode;
    }
}
