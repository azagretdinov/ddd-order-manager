package com.bitbucket.azagretdinov.dddordermanager.portfolio.model;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class CashAccount {
    private final String sortCode;
    private final String accountNumber;

    public CashAccount(String sortCode, String accountNumber) {
        this.sortCode = sortCode;
        this.accountNumber = accountNumber;
    }
}
