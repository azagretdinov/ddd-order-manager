package com.bitbucket.azagretdinov.dddordermanager.portfolio.event;

public interface PortfolioEvent {
    String getPortfolioReferenceIdentifier();
    String getType();
}
