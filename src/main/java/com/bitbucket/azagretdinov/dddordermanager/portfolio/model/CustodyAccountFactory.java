package com.bitbucket.azagretdinov.dddordermanager.portfolio.model;

import org.springframework.stereotype.Component;

@Component
public class CustodyAccountFactory {
    public CustodyAccount createCustodyAccount(String custodyAccountNumber) {
        return new CustodyAccount(custodyAccountNumber);
    }
}
