package com.bitbucket.azagretdinov.dddordermanager.portfolio.controller;

import com.bitbucket.azagretdinov.dddordermanager.order.event.OrderCashSecured;
import com.bitbucket.azagretdinov.dddordermanager.order.event.OrderEvent;
import com.bitbucket.azagretdinov.dddordermanager.order.event.OrderSent;
import com.bitbucket.azagretdinov.dddordermanager.order.messaging.OrderBindings;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.event.PortfolioEvent;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.OrderInstruction;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.service.PortfolioService;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

@RestController
@Slf4j
public class PortfolioController {

    private final PortfolioService portfolioService;
    private final SubscribableChannel orderEvents;

    public PortfolioController(PortfolioService portfolioService, OrderBindings orderBindings) {
        this.portfolioService = portfolioService;
        this.orderEvents = orderBindings.orderEvents();
    }

    @PutMapping(
            value = "/portfolio/{portfolioReference}/order/{orderId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_STREAM_JSON_VALUE
    )
    public Flux<OrderPortfolioEvent> createOrder(
            @PathVariable("portfolioReference") String portfolioReference,
            @PathVariable("orderId") String orderId,
            @RequestBody OrderInstruction orderInstruction
    ){
        return Flux.create(
                sink -> {
                    final OrderEventSubscriber orderEventSubscriber = new OrderEventSubscriber(portfolioReference, orderId, sink);
                    orderEvents.subscribe(orderEventSubscriber);
                    sink.onDispose(() -> orderEvents.unsubscribe(orderEventSubscriber));
                    portfolioService.placeOrder(portfolioReference, orderInstruction);
                }
        );
    }

    public static class OrderEventSubscriber implements MessageHandler {
        private final String portfolioReference;
        private final String orderId;
        private final FluxSink<OrderPortfolioEvent> sink;

        private OrderEventSubscriber(String portfolioReference, String orderId, FluxSink<OrderPortfolioEvent> sink) {
            this.portfolioReference = portfolioReference;
            this.orderId = orderId;
            this.sink = sink;
        }

        @Override
        public void handleMessage(Message<?> message) throws MessagingException {
            final Object messageOrderId = message.getHeaders().get("orderId");
            log.info("receive event: {}", message.getPayload());
            if(orderId.equals(messageOrderId)){
                final OrderEvent event = (OrderEvent) message.getPayload();
                sink.next(new OrderPortfolioEvent(portfolioReference, event));
                if(event instanceof OrderSent){
                    sink.complete();
                }
            }
        }
    }

    @ToString
    public static class OrderPortfolioEvent implements PortfolioEvent {
        private final String orderId;
        private final OrderEvent orderEvent;
        private final String portfolioReference;

        public OrderPortfolioEvent(String portfolioReference, OrderEvent orderEvent) {
            this.portfolioReference = portfolioReference;
            this.orderEvent = orderEvent;
            this.orderId = orderEvent.getOrderId();
        }

        @Override
        public String getPortfolioReferenceIdentifier() {
            return portfolioReference;
        }

        @Override
        public String getType() {
            return orderEvent.getType();
        }

        public String getOrderId() {
            return orderId;
        }

        public OrderEvent getOrderEvent() {
            return orderEvent;
        }
    }
}
