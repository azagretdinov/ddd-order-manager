package com.bitbucket.azagretdinov.dddordermanager.portfolio.model;

import lombok.Getter;

@Getter
public class CustodyAccount {
    private String custodyAccountNumber;

    public CustodyAccount(String custodyAccountNumber) {
        this.custodyAccountNumber = custodyAccountNumber;
    }
}
