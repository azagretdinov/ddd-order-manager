package com.bitbucket.azagretdinov.dddordermanager.portfolio.repository;

import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.Portfolio;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Repository
public class PortfolioRepository {

    private final ConcurrentMap<String, Portfolio> data;

    public PortfolioRepository() {
        data = new ConcurrentHashMap<>();
    }

    public void store(Portfolio portfolio) {
        data.put(portfolio.getReferenceIdentifier(), portfolio);
    }

    public Portfolio restore(String referenceIdentifier) {
        return data.get(referenceIdentifier);
    }
}
