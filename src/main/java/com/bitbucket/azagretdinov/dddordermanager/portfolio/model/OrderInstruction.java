package com.bitbucket.azagretdinov.dddordermanager.portfolio.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Currency;

@Getter
@ToString
public class OrderInstruction {

    private final String orderId;
    private final String symbol;
    private final BigDecimal quantity;
    private final Currency currency;

    @JsonCreator
    public OrderInstruction(
            final @JsonProperty("orderId") String orderId,
            final @JsonProperty("symbol") String symbol,
            final @JsonProperty("quantity") BigDecimal quantity,
            final @JsonProperty("currency") Currency currency
    ) {
        this.orderId = orderId;
        this.symbol = symbol;
        this.quantity = quantity;
        this.currency = currency;
    }
}
