package com.bitbucket.azagretdinov.dddordermanager.portfolio.model;

import com.bitbucket.azagretdinov.dddordermanager.portfolio.event.OrderPlaced;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.event.OrderRejected;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.event.PortfolioEvent;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@ToString
public class Portfolio {
    private final String referenceIdentifier;
    private final CashAccount cashAccount;
    private final CustodyAccount custodyAccount;
    private final ClientRiskRate clientRiskRate;
    private final PortfolioRiskRate portfolioRiskRate;
    private final List<String> orders;


    public Portfolio(
            String referenceIdentifier, CashAccount cashAccount, CustodyAccount custodyAccount,
            ClientRiskRate clientRiskRate, PortfolioRiskRate portfolioRiskRate
    ) {

        this.referenceIdentifier = referenceIdentifier;
        this.cashAccount = cashAccount;
        this.custodyAccount = custodyAccount;
        this.clientRiskRate = clientRiskRate;
        this.portfolioRiskRate = portfolioRiskRate;
        this.orders = new ArrayList<>();
    }

    public String getReferenceIdentifier() {
        return referenceIdentifier;
    }

    public PortfolioEvent placeOrder(final OrderInstruction orderInstruction) {
        if(orders.contains(orderInstruction.getOrderId())){
            return new OrderRejected(referenceIdentifier, orderInstruction);
        }
        orders.add(orderInstruction.getOrderId());
        return new OrderPlaced(
                referenceIdentifier, orderInstruction,
                LocalDate.now(), LocalTime.now(),
                cashAccount, custodyAccount
        );
    }
}
