package com.bitbucket.azagretdinov.dddordermanager.portfolio.event;


import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CashAccount;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CustodyAccount;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.OrderInstruction;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@ToString
public class OrderPlaced implements PortfolioEvent {
    private final String portfolioReferenceIdentifier;
    private final OrderInstruction orderInstruction;
    private final LocalDate placeDate;
    private final LocalTime placeTime;
    private final CashAccount cashAccount;
    private final CustodyAccount custodyAccount;

    @JsonCreator
    public OrderPlaced(
            @JsonProperty("portfolioReferenceIdentifier") String portfolioReferenceIdentifier,
            @JsonProperty("orderInstruction") OrderInstruction orderInstruction,
            @JsonProperty("placeDate") LocalDate placeDate,
            @JsonProperty("placeTime") LocalTime placeTime,
            @JsonProperty("cashAccount") CashAccount cashAccount,
            @JsonProperty("custodyAccount") CustodyAccount custodyAccount
    ) {
        this.portfolioReferenceIdentifier = portfolioReferenceIdentifier;
        this.orderInstruction = orderInstruction;
        this.placeDate = placeDate;
        this.placeTime = placeTime;
        this.cashAccount = cashAccount;
        this.custodyAccount = custodyAccount;
    }

    @Override
    public String getPortfolioReferenceIdentifier() {
        return portfolioReferenceIdentifier;
    }

    @Override
    public String getType() {
        return "order-placed";
    }
}
