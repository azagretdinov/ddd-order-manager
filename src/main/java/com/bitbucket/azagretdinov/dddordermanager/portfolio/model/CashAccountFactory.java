package com.bitbucket.azagretdinov.dddordermanager.portfolio.model;

import org.springframework.stereotype.Component;

@Component
public class CashAccountFactory {
    public CashAccount createCashAccount(String sortCode, String accountNumber) {
        return new CashAccount(sortCode, accountNumber);
    }
}
