package com.bitbucket.azagretdinov.dddordermanager.portfolio.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface PortfolioBindings {
    String PORTFOLIO_EVENTS = "order-portfolio-events";

    @Input(PORTFOLIO_EVENTS)
    SubscribableChannel portfolioEvents();
}
