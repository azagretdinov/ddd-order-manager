package com.bitbucket.azagretdinov.dddordermanager.portfolio.event;

import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.OrderInstruction;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class OrderRejected implements PortfolioEvent {
    private final String portfolioReferenceIdentifier;
    private final OrderInstruction orderInstruction;

    @JsonCreator
    public OrderRejected(@JsonProperty("portfolioReferenceIdentifier") String portfolioReferenceIdentifier,
                         @JsonProperty("orderInstruction") final OrderInstruction orderInstruction) {
        this.portfolioReferenceIdentifier = portfolioReferenceIdentifier;
        this.orderInstruction = orderInstruction;
    }

    @Override
    public String getPortfolioReferenceIdentifier() {
        return portfolioReferenceIdentifier;
    }

    @Override
    public String getType() {
        return "order-rejected";
    }
}
