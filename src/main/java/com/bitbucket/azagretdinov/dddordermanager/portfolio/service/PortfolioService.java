package com.bitbucket.azagretdinov.dddordermanager.portfolio.service;

import com.bitbucket.azagretdinov.dddordermanager.portfolio.event.PortfolioEvent;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.messaging.PortfolioBindings;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CashAccountDetails;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CustodyAccountDetails;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.OrderInstruction;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.Portfolio;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.PortfolioFactory;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.repository.PortfolioRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PortfolioService {
    private final PortfolioFactory portfolioFactory;
    private final PortfolioRepository portfolioRepository;
    private final MessageChannel eventsChannel;

    public PortfolioService(PortfolioFactory portfolioFactory, PortfolioRepository portfolioRepository, PortfolioBindings portfolioBindings) {
        this.portfolioFactory = portfolioFactory;
        this.portfolioRepository = portfolioRepository;
        eventsChannel = portfolioBindings.portfolioEvents();
    }

    public void newPortfolio(
            String referenceIdentifier, CashAccountDetails cashAccountDetails,
            CustodyAccountDetails custodyAccountDetails, String clientRiskRate, String portfolioRiskRate
    ) {
        final Portfolio portfolio = portfolioFactory.createPortfolio(
                referenceIdentifier,
                cashAccountDetails,
                custodyAccountDetails,
                clientRiskRate,
                portfolioRiskRate
        );
        portfolioRepository.store(portfolio);
    }

    public void placeOrder(final String referenceIdentifier, final OrderInstruction orderInstruction){
        log.info("Receive order instruction {} for portfolio {}", orderInstruction, referenceIdentifier);
        final Portfolio portfolio = portfolioRepository.restore(referenceIdentifier);
        final PortfolioEvent event = portfolio.placeOrder(orderInstruction);
        publish(event);
    }

    private void publish(PortfolioEvent event) {
        final Message<PortfolioEvent> message = MessageBuilder.withPayload(event)
                .setHeader("eventType", event.getType())
                .build();
        log.info("publishing {}", message);
        eventsChannel.send(message);
    }
}
