package com.bitbucket.azagretdinov.dddordermanager.portfolio.model;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PortfolioFactory {

    private final CashAccountFactory cashAccountFactory;
    private final CustodyAccountFactory custodyAccountFactory;

    public PortfolioFactory(CashAccountFactory cashAccountFactory, CustodyAccountFactory custodyAccountFactory) {
        this.cashAccountFactory = cashAccountFactory;
        this.custodyAccountFactory = custodyAccountFactory;
    }

    public Portfolio createPortfolio(
            String referenceIdentifier, CashAccountDetails cashAccountDetails, CustodyAccountDetails custodyAccountDetails,
            String clientRiskRate, String portfolioRiskRate
    ) {
        final Portfolio portfolio = new Portfolio(
                referenceIdentifier,
                cashAccountFactory.createCashAccount(cashAccountDetails.getSortCode(), cashAccountDetails.getAccountNumber()),
                custodyAccountFactory.createCustodyAccount(custodyAccountDetails.getCustodyAccountNumber()),
                clientRiskRate != null ? ClientRiskRate.valueOf(clientRiskRate) : null,
                portfolioRiskRate != null ? PortfolioRiskRate.valueOf(portfolioRiskRate) : null
        );
        log.info("Portfolio created: {}", portfolio);
        return portfolio;
    }
}
