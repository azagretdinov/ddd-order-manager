package com.bitbucket.azagretdinov.dddordermanager.order.command;

public interface OrderCommand {
    String getOrderId();
    String getType();
}
