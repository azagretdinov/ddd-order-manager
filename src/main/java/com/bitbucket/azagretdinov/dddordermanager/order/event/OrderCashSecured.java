package com.bitbucket.azagretdinov.dddordermanager.order.event;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class OrderCashSecured  implements OrderEvent {
    private final String orderId;
    private final BigDecimal cashAmount;

    public OrderCashSecured(String orderId, BigDecimal cashAmount) {
        this.orderId = orderId;
        this.cashAmount = cashAmount;
    }

    @Override
    public String getType() {
        return "order-cash-secured";
    }
}
