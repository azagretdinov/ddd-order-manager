package com.bitbucket.azagretdinov.dddordermanager.order.command;

import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CashAccount;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@ToString
public class SecureCash implements OrderCommand{

    private final String orderId;
    private final CashAccount cashAccount;
    private final BigDecimal cashAmount;

    @JsonCreator
    public SecureCash(
            @JsonProperty("orderId") String orderId,
            @JsonProperty("cashAccount") CashAccount cashAccount,
            @JsonProperty("cashAmount") BigDecimal cashAmount) {
        this.orderId = orderId;
        this.cashAccount = cashAccount;
        this.cashAmount = cashAmount;
    }

    @Override
    public String getType() {
        return "secure-cash";
    }
}
