package com.bitbucket.azagretdinov.dddordermanager.order.event;

import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CashAccount;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CustodyAccount;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.OrderInstruction;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
public class OrderCreated implements OrderEvent {
    private final String orderId;
    private final LocalDate createDate;
    private final LocalTime createTime;
    private final OrderInstruction orderInstruction;
    private final CashAccount cashAccount;
    private final CustodyAccount custodyAccount;


    public OrderCreated(String orderId, LocalDate createDate, LocalTime createTime,
                        OrderInstruction orderInstruction, CashAccount cashAccount,
                        CustodyAccount custodyAccount) {
        this.orderId = orderId;
        this.createDate = createDate;
        this.createTime = createTime;
        this.orderInstruction = orderInstruction;
        this.cashAccount = cashAccount;
        this.custodyAccount = custodyAccount;
    }

    @Override
    public String getType() {
        return "order-created";
    }
}
