package com.bitbucket.azagretdinov.dddordermanager.order.command;

import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.OrderInstruction;
import lombok.Getter;

@Getter
public class SendOrder implements OrderCommand{

    private final String orderId;
    private final OrderInstruction orderInstruction;

    public SendOrder(String orderId, OrderInstruction orderInstruction) {
        this.orderId = orderId;
        this.orderInstruction = orderInstruction;
    }

    @Override
    public String getType() {
        return "send-order";
    }
}
