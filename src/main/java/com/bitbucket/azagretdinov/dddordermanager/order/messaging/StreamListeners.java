package com.bitbucket.azagretdinov.dddordermanager.order.messaging;

import com.bitbucket.azagretdinov.dddordermanager.anticorruption.bank.event.CashSecured;
import com.bitbucket.azagretdinov.dddordermanager.anticorruption.bank.messaging.BankBindings;
import com.bitbucket.azagretdinov.dddordermanager.order.command.CreateOrder;
import com.bitbucket.azagretdinov.dddordermanager.order.service.OrderService;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.event.OrderPlaced;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.messaging.PortfolioBindings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component("orderStreamListeners")
public class StreamListeners {

    private final OrderService orderService;

    public StreamListeners(OrderService orderService) {
        this.orderService = orderService;
    }

    @StreamListener(
            target= PortfolioBindings.PORTFOLIO_EVENTS,
            condition = "headers['eventType']=='order-placed'"
    )
    public void orderPlaced(OrderPlaced orderPlaced){
        log.info("Order placed: {}", orderPlaced.getOrderInstruction());
        orderService.handle(
                new CreateOrder(
                        orderPlaced.getOrderInstruction().getOrderId(),
                        orderPlaced.getPlaceDate(),
                        orderPlaced.getPlaceTime(),
                        orderPlaced.getCashAccount(),
                        orderPlaced.getCustodyAccount(),
                        orderPlaced.getOrderInstruction()
                )
        );
    }

    @StreamListener(
            target= BankBindings.BANK_EVENTS,
            condition = "headers['eventType']=='cash-secured'"
    )
    public void cashSecured(CashSecured cashSecured){
        log.info("cash secured: {}", cashSecured);
        orderService.handle(cashSecured);
    }
}
