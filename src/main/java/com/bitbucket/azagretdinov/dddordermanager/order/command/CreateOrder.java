package com.bitbucket.azagretdinov.dddordermanager.order.command;

import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CashAccount;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CustodyAccount;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.OrderInstruction;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@ToString
public class CreateOrder implements OrderCommand {

    private final String orderId;
    private final LocalDate createDate;
    private final LocalTime createTime;
    private final CashAccount cashAccount;
    private final CustodyAccount custodyAccount;
    private final OrderInstruction orderInstruction;


    public CreateOrder(
            String orderId,
            LocalDate createDate,
            LocalTime createTime,
            CashAccount cashAccount, CustodyAccount custodyAccount, OrderInstruction orderInstruction
    ) {
        this.orderId = orderId;
        this.createDate = createDate;
        this.createTime = createTime;
        this.cashAccount = cashAccount;
        this.custodyAccount = custodyAccount;
        this.orderInstruction = orderInstruction;
    }

    @Override
    public String getType() {
        return "create-order";
    }
}
