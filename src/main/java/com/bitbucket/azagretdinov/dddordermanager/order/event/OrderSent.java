package com.bitbucket.azagretdinov.dddordermanager.order.event;

public class OrderSent implements OrderEvent {
    private String orderId;

    public OrderSent(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String getOrderId() {
        return orderId;
    }

    @Override
    public String getType() {
        return "order-sent";
    }
}
