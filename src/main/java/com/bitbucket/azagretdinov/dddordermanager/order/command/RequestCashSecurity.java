package com.bitbucket.azagretdinov.dddordermanager.order.command;

public class RequestCashSecurity implements OrderCommand {
    private String orderId;

    public RequestCashSecurity(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String getOrderId() {
        return orderId;
    }

    @Override
    public String getType() {
        return "request-cash-security";
    }
}
