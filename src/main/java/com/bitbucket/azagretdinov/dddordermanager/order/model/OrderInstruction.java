package com.bitbucket.azagretdinov.dddordermanager.order.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Currency;

@Getter
@ToString
public class OrderInstruction {

    private String orderId;
    private String symbol;
    private BigDecimal quantity;
    private Currency currency;

    @JsonCreator
    public OrderInstruction(
            final @JsonProperty("orderId") String orderId,
            final @JsonProperty("symbol") String symbol,
            final @JsonProperty("quantity") BigDecimal quantity,
            final @JsonProperty("currency") Currency currency
    ) {
        this.orderId = orderId;
        this.symbol = symbol;
        this.quantity = quantity;
        this.currency = currency;
    }
}
