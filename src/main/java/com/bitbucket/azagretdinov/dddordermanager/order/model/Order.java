package com.bitbucket.azagretdinov.dddordermanager.order.model;

import com.bitbucket.azagretdinov.dddordermanager.anticorruption.bank.event.CashSecured;
import com.bitbucket.azagretdinov.dddordermanager.order.command.CreateOrder;
import com.bitbucket.azagretdinov.dddordermanager.order.command.OrderCommand;
import com.bitbucket.azagretdinov.dddordermanager.order.command.SecureCash;
import com.bitbucket.azagretdinov.dddordermanager.order.command.SendOrder;
import com.bitbucket.azagretdinov.dddordermanager.order.event.OrderCashSecured;
import com.bitbucket.azagretdinov.dddordermanager.order.event.OrderCreated;
import com.bitbucket.azagretdinov.dddordermanager.order.event.OrderEvent;
import com.bitbucket.azagretdinov.dddordermanager.order.event.OrderSent;
import com.bitbucket.azagretdinov.dddordermanager.order.service.QuoteService;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CashAccount;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.CustodyAccount;
import com.bitbucket.azagretdinov.dddordermanager.portfolio.model.OrderInstruction;
import lombok.ToString;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@ToString
public class Order {

    public static Order newOrder(String orderId, QuoteService quoteService) {
        return new Order(orderId, quoteService);
    }

    private List<OrderEvent> pendingEvents;
    private List<OrderCommand> pendingCommands;


    private BigDecimal expectedCashAmount;


    private OrderInstruction orderInstruction;
    private CashAccount cashAccount;
    private CustodyAccount custodyAccount;
    private LocalDate createDate;
    private LocalTime createTime;

    private BigDecimal riskCoefficient;

    private final String orderId;
    private final QuoteService quoteService;

    private Status status;

    private Order(String orderId, QuoteService quoteService) {
        this.orderId = orderId;
        this.quoteService = quoteService;
        riskCoefficient = BigDecimal.valueOf(0.10);
        pendingEvents = new ArrayList<>();
        pendingCommands = new ArrayList<>();
    }

    public String getOrderId() {
        return orderId;
    }

    public List<OrderEvent> getPendingEvents() {
        return new ArrayList<>(pendingEvents);
    }

    public void clearPendingEvents(){
        pendingEvents.clear();
    }

    public List<OrderCommand> getPendingCommands() {
        return new ArrayList<>(pendingCommands);
    }

    public void clearPendingCommands(){
        pendingCommands.clear();
    }

    public void process(CreateOrder createOrder) {
        final OrderCreated orderCreated = new OrderCreated(
                createOrder.getOrderId(),
                createOrder.getCreateDate(), createOrder.getCreateTime(),
                createOrder.getOrderInstruction(),
                createOrder.getCashAccount(),
                createOrder.getCustodyAccount()
        );
        pendingEvents.add(orderCreated);
        apply(orderCreated);
        requestCashSecurity();
    }

    private void requestCashSecurity() {
        final BigDecimal expectedCashAmount = applyRisks(
                calculateExpectedCashAmount()).round(new MathContext(2)
        );
        pendingCommands.add(new SecureCash(orderId, cashAccount, expectedCashAmount));
    }

    private BigDecimal applyRisks(BigDecimal cashAmount) {
        return cashAmount.add(
                cashAmount.multiply(riskCoefficient)
        );
    }

    private BigDecimal calculateExpectedCashAmount() {
        return quoteService.lastPrice(orderInstruction.getSymbol())
                .multiply(orderInstruction.getQuantity());
    }

    public void process(CashSecured cashSecured) {
        if (status == Status.New) {
            cashSecured(cashSecured);
            sendOrder();
        }
    }

    private void sendOrder() {
        pendingCommands.add(new SendOrder(orderId, orderInstruction));
        final OrderSent orderSent = new OrderSent(orderId);
        pendingEvents.add(orderSent);
        apply(orderSent);
    }

    private void cashSecured(CashSecured cashSecured) {
        final OrderCashSecured orderCashSecured = new OrderCashSecured(orderId, cashSecured.getCashAmount());
        pendingEvents.add(orderCashSecured);
        apply(orderCashSecured);
    }

    public void apply(OrderCreated orderCreated) {
        status = Status.New;

        orderInstruction = orderCreated.getOrderInstruction();
        cashAccount = orderCreated.getCashAccount();
        custodyAccount = orderCreated.getCustodyAccount();
        createDate = orderCreated.getCreateDate();
        createTime = orderCreated.getCreateTime();
    }

    public void apply(OrderCashSecured cashSecured) {
        status = Status.CashSecured;
        expectedCashAmount = cashSecured.getCashAmount();
    }

    public void apply(OrderSent orderSent) {
        pendingEvents.add(orderSent);
        status = Status.OrderSent;
    }

    public void apply(OrderEvent orderEvent) {
        if (orderEvent instanceof OrderCreated) {
            apply((OrderCreated) orderEvent);
        } else if (orderEvent instanceof OrderCashSecured) {
            apply((OrderCashSecured) orderEvent);
        } else if (orderEvent instanceof OrderSent) {
            apply((OrderSent) orderEvent);
        }
    }

    private enum Status {
        New,
        CashSecured,
        OrderSent
    }
}
