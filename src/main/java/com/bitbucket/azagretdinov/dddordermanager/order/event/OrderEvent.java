package com.bitbucket.azagretdinov.dddordermanager.order.event;

public interface OrderEvent {
    String getOrderId();
    String getType();
}
