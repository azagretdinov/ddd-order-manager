package com.bitbucket.azagretdinov.dddordermanager.order.service;

import com.bitbucket.azagretdinov.dddordermanager.anticorruption.bank.event.CashSecured;
import com.bitbucket.azagretdinov.dddordermanager.order.command.CreateOrder;
import com.bitbucket.azagretdinov.dddordermanager.order.command.OrderCommand;
import com.bitbucket.azagretdinov.dddordermanager.order.messaging.OrderBindings;
import com.bitbucket.azagretdinov.dddordermanager.order.model.Order;
import com.bitbucket.azagretdinov.dddordermanager.order.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final QuoteService quoteService;
    private final MessageChannel orderCommands;

    public OrderService(OrderRepository orderRepository, QuoteService quoteService, OrderBindings orderBindings) {
        this.orderRepository = orderRepository;
        this.quoteService = quoteService;
        orderCommands = orderBindings.orderCommands();
    }

    public void handle(CreateOrder createOrder) {
        final Order order = Order.newOrder(createOrder.getOrderId(), quoteService);
        log.info("Order created: {}", order);
        order.process(createOrder);
        store(order).subscribe();
    }

    public void handle(CashSecured cashSecured) {
        log.info("CashSecured: {}", cashSecured);
        orderRepository.getOrder(cashSecured.getOrderId())
                .doOnNext(order -> order.process(cashSecured))
                .flatMap(this::store)
                .log(log.getName())
                .subscribe();
    }

    private Mono<Void> store(Order order) {
        return orderRepository.store(order)
                .log(log.getName())
                .map(this::prepareCommands)
                .doOnNext(this::publish)
                .log(log.getName())
                .then(Mono.empty());
    }

    private List<OrderCommand> prepareCommands(Order order) {
        final List<OrderCommand> pendingCommands = order.getPendingCommands();
        order.clearPendingCommands();
        return pendingCommands;
    }

    private void publish(List<OrderCommand> orderCommands) {
        orderCommands.forEach(this::publish);
    }

    private void publish(OrderCommand orderCommand) {
        final Message<OrderCommand> message = MessageBuilder.withPayload(orderCommand)
                .setHeader("orderId", orderCommand.getOrderId())
                .setHeader("eventType", orderCommand.getType())
                .build();
        log.info("publishing command {}", message);
        orderCommands.send(message);
    }
}
