package com.bitbucket.azagretdinov.dddordermanager.order.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;


public interface OrderBindings {

    String ORDER_EVENTS = "order-events";
    String ORDER_COMMAND = "order-commands";

    @Input(ORDER_EVENTS)
    SubscribableChannel orderEvents();

    @Input(ORDER_COMMAND)
    SubscribableChannel orderCommands();
}
