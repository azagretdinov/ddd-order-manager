package com.bitbucket.azagretdinov.dddordermanager.order.repository;

import com.bitbucket.azagretdinov.dddordermanager.order.event.OrderEvent;
import com.bitbucket.azagretdinov.dddordermanager.order.messaging.OrderBindings;
import com.bitbucket.azagretdinov.dddordermanager.order.model.Order;
import com.bitbucket.azagretdinov.dddordermanager.order.service.QuoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

@Slf4j
@Repository
public class OrderRepository {

    private final ExecutorService executorService;
    private final ConcurrentMap<String, List<OrderEvent>> data;
    private final QuoteService quoteService;
    private final SubscribableChannel eventsChannel;

    public OrderRepository(QuoteService quoteService, OrderBindings orderBindings) {
        this.quoteService = quoteService;
        executorService = Executors.newSingleThreadExecutor();
        data = new ConcurrentHashMap<>();
        eventsChannel = orderBindings.orderEvents();
    }

    public Mono<Order> store(Order order) {
        return Mono.fromFuture(
                CompletableFuture.supplyAsync(doStore(order), executorService)
        );
    }

    private Supplier<Order> doStore(Order order) {
        return () -> {
            final List<OrderEvent> events = data.getOrDefault(order.getOrderId(), new ArrayList<>());
            final List<OrderEvent> pendingEvents = order.getPendingEvents();
            order.clearPendingEvents();
            events.addAll(pendingEvents);
            data.put(order.getOrderId(), events);
            log.info("order stored: {}", order);
            publish(pendingEvents);
            return order;
        };
    }

    private void publish(List<OrderEvent> pendingEvents) {
        pendingEvents.forEach(this::publish);
    }


    private void publish(OrderEvent orderEvent) {
        final Message<OrderEvent> message = MessageBuilder.withPayload(orderEvent)
                .setHeader("orderId", orderEvent.getOrderId())
                .setHeader("eventType", orderEvent.getType())
                .build();
        log.info("publishing event {}", message);
        eventsChannel.send(message);
    }

    public Mono<Order> getOrder(String orderId) {
        return Mono.fromFuture(
                CompletableFuture.supplyAsync(doGetStore(orderId), executorService)
        );
    }

    private Supplier<Order> doGetStore(String orderId) {
        return () -> {
            final List<OrderEvent> events = data.getOrDefault(orderId, new ArrayList<>());
            final Order order = Order.newOrder(orderId, quoteService);
            events.forEach(order::apply);
            log.info("order restored: {}", order);
            return order;
        };
    }
}
